FROM golang:latest

RUN mkdir /app
ADD hand-on/h02 /app/
WORKDIR /app
RUN go build -ldflags "-X main.buildcommit=`git rev-parse --short HEAD` -X main.buildtime=`date "+%Y-%m-%dT%H:%M:%S%Z:00"`" -o main
CMD ["/app/main"]

EXPOSE 8081
ENV HOST 0.0.0.0
