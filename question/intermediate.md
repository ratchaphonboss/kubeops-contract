# Assume we have an application that is designed as below. Our application stopped responding due to an extremely high number of clients in some circumstances.
- We have tried scaling a number of API Gateway and Service A nodes but it didn’t help. What are the possible problems that lies in our system in which components and how to fix them?
```javascript
ต้องมองที่ภาพใหญ่ว่าปัญหาเกิดจากอะไรบ้าง ของ service
1. ระบบได้มีการทำ microservice แล้วหรือยัง ?
ในกรณีที่เราออกแบบเป็น microservice มีการเพิ่มจำนวน nodes มากขึ้นแต่ยังไม่ได้ช่วย มองได้ x กรณี
    1.1 จำนวน request มีมากจำนวนมาก service ที่ใช้ process และ response ทำงานไม่ทัน
        1.1.2 เพิ่ม Kafka สำหรับ message Q เพื่อ scale ให้รองรับจำนวนมากขึ้น 
        1.1.2 เพิ่มจำนวน service ที่ใช้ process และ response มากขึ้น และเพิ่มจำนวน partition ตามจำนวน service จะทำให้การ process ทำงานได้เร็วขึ้น
    1.2 database ได้มีการทำ replica แยก เขียน/อ่าน หรือยัง ?
    1.3 ขนาด server เพียงพอหรือไม่ควรต้อง horizontal scaling แล้วหรือยัง ?
    1.4 ลูกค้า request ข้อมูลซ้ำๆกัน ในช่วงเวลาใกล้เคียงกันหรือไม่ ?
        1.4.1 ควรใช้ redis สำหรับการทำ cache
        1.4.1 ถ้าต้องการเร็วมากๆ ทุกครั้งที่มีการเขียนข้อมูลลง DB เรา clone ข้อมูลใส่ redis แล้วให้ api ดึงตรงจาก redis 
    1.5 ภาษาที่ใช้พัฒนา api ?
        1.5.1 ควรใช้งาน golang ที่จะช่วยลดการใช้งาน ram ทำให้ประหยัดต้นทุน และมีความเร็วที่สูง
    1.6 คุณใช้งาน k8s หรือไม่ ? ไม่งั้นจะยากต้องการบริหาร cluster แต่ถึงใช้งานก็ อาจจะทำให้ระบบล่มได้ เพราะ scale ไม่ทัน ควรรู้ช่วงเวลาที่จะมี Traffic สูลและทำการ scale ไว้รอดีที่สุด เพราะมี case เป๋าตังที่ใช้งาน k8s แต่ระบบจะล่มอยู่ช่วงเวลานึง เพราะ scale ไม่ทัน
```

# How do you keep the docker image smallest as possible?
- เริ่มจากการ เขียน Dockerfile

```javascript
ในแง่ของขนาด ที่เราลดได้อย่างง่าย แต่แง่ของการ debug จะยากเพราะไม่รู้ว่า error อะไรแน่ๆ และความเร็วในการ build

RUN apt-get update
RUN xxx-1
RUN xxx-2
RUN xxx-3

แก้เป็น

RUN apt-get update xxx-1 xxx-2 xxx-3
```

```javascript
การเรียก alpine มีผลทางตรงด้านขนาด

FROM node:8-alpine
```

โดยสรุปการลดที่ดีคือควบคุมเรื่อง Caching

# How does the Kubernetes service talk to each other in the same cluster?
สร้าง ClusterIP แล้วคุยกันด้วยชื่อของ SVC ไม่แนะนำให้ใช้งาน ip address และ port เนื่องจากมีโอกาศเปลี่ยนได้

# What’s different between L2, L4, and L7 Load balancers? When to use it?
ต้องขออภัยครับ ไม่ทราบเรื่องนี้จริงๆ ต้องขอให้พี่ๆช่วยสอนด้วยครับ
