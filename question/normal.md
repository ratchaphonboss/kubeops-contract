## Describe Git branching strategies (Git-flow, single branch, feature branch etc.) which you have used and what purpose does it serves.

- Main Branches: ในทีม Dev เรามีการตกลงกันว่าเราจะมี Env อะไรบ้างก่อนนำโค้ดขึ้น 
```javascript
+ production branch โดยเราจะมีการกำหนด master สำหรับขึ้น production หลังจาก review --> sec scan --> unit test --> load test --> deploy
+ develop branch สำหรับขึ้น development ภายใน local หลังจาก review --> sec scan --> unit test --> load test --> deploy
```
- Supporting Branches: โดยมีการแบ่งออกเป็น 3 branchs 
```javascript
+ feature branch เรานิยมที่เวลาโค้ดของเรามีฟังก์ชันใหม่ขึ้นมา ควรแตก branch ออกมาจาก develop branch เสมอ จากนั้นทำการ merge กลับ develop branch เช่นเดิม
+ release branch ก่อนจะ merge develop branch ไปที่ master branch เราจะมีการ merge develop branch ไปที่ release branch ก่อนเสมอ จากนั้นทำการ review --> sec scan --> unit test --> load test --> merge ไปที่ master branch จากนั้นสร้าง Tag version
+ hotfix branch ในกรณีที่เกิดปัญหาขึ้นใน master branch เราสามารถ merge master branch กลับมาที่ hotfix branch เพื่อทำการแก้ไข จากนั้นกลับ loop เดิมคือไปที่ merge กลับ develop branch
```

## How do you revert a commit that has already been pushed and made public?
git reset <commit_hash> จากนั้น git push -f

## How do you normally solve conflicts in a feature branch before merge?
ใช้ vs code ช่วยเลือกว่าบรรทัดนี้จะเอาข้อมูลก่อน หรือหลัง

## “200 OK” what does it mean and show use case this HTTP Status?
โดยปกติมีความหมายว่า success หรือเรานิยม code 200 สำหรับเติม ในกรณีมี error message ที่ต้องการระบุปัญหาที่เกิดขึ้น และง่ายต่อความเข้าใจ
```javascript
{
     "header": {
           "code": 1111,
           "description": "GPU is Down."
     }
}
```
## “201 Created” what does it mean and show use case this HTTP Status?
ยกตัวอย่าง ในกรณีที่เราต้องการสร้าง order สินค้า จะมีการยิง POST ในกรณีเขียน Order ลง DB สำเร็จ จะ response ด้วย 201 Status

## “301 Moved Permanently” what does it mean and show use case this HTTP Status?
ใช้สำหรับการย้าย URL ไปอยู่ที่ตำแหน่งใหม่อย่างถาวร เพื่อไม่ให้ต่อ Google SEO

## “400 Bad Request” what does it mean and how to identify the problem?
เกิดได้ 2 กรณี 
- ลูกค้าใช้ URL ผิดเอง
- ทางเรามีการตั้งค่า Ingress ผิดเส้นทาง

## “401 Unauthorized” what does it mean and how to identify the problem?
ใช้ในกรณีที่ต้องการ login แต่ลูกค้ากรอกข้อมูลผิด เช่น email หรือ password หรือ OTP ไม่อยู่ในกำหนดเวลา

## “403 Forbidden” what does it mean and how to identify the problem?
การใช้งาน WAF สำหรับ ป้องกัน การเข้าถึงไฟล์ เข้าถึงข้อมูล หรือเว็บไซต์

## 404 Not Found” what does it mean and how to identify the problem?
เกิดได้ 2กรณี
- มีการลบหน้าเพจทิ้ง
- มีการเปลี่ยนชื่อ URL

## “500 Internal Server Error” what does it mean and how to identify the problem?
เกิดได้เบื้องต้น 2 กรณี
- ปัญหาจาก server 
- ปัญหาจาก web server 

## “502 Bad Gateway” what does it mean and how to identify the problem?
เกิดได้หลายสาเหตุ
- อาจจะโดนโจมตีด้วย DDoS
- ปัญหาจากการ scale ไม่ทันกับจำนวนผู้ใช้งาน
- โค้ดของเราเกิด Bug จากการ release
- เน็ตฝั่งลูกค้าหลุด

## “503 Service Unavailable” what does it mean and how to identify the problem?
- มีการ request จำนวนมาก
- server กำลังบำรุงรักษา

## “504 Gateway Timeout” what does it mean and how to identify the problem?
ยกตัวอย่างเช่น เว็บไซต์หนึ่งมีความสามารถในการรองรับผู้ใช้งานพร้อมกันในเวลาเดียว 10,000 user แต่อยู่ดีๆ มีคนเข้ามาใช้งานในเว็บไซต์ในเวลานั้นพร้อมกัน 20,000 User ทำให้ระบบ Server ไม่สามารถรองรับได้ จนเกิดกรณี 504 Geteway time-out

## What are Linux network tools do you use for troubleshooting network problems as well as usage scenarios for each tool?
- ในกรณีที่ logs monitor ควรใช้ ELK Stack มาช่วยในการวิเคราะห์เพื่อหาสาเหตุ
- ในกรณีที่ physical monitor ควรใช้ TICK Stack ช่วย monitor และ alert
- ในกรณีที่ใช้งาน k8s หากใช้เครื่องมือ kubectl ควรดูจากระดับ pod ขึ้นมาจนถึง ingress



