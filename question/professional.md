# Assume that you are using a private cloud for your infrastructure. How do you manage logs, metrics, and alerts for your infrastructure and applications? Which tools do you use and why?
ผมมองจากประสบการณ์
```javascript
1. ใช้งาน TICK Stack ช่วย monitor และ alert metrics ต่างๆ ข้อดีคือมี ml ช่วย predict ข้อมูลได้
2. ELK Stack มาช่วยในการวิเคราะห์เพื่อหาสาเหตุ จาก log รวมถึงการหาความสัมพันธ์ในรูปแบบ graph analysis ได้อีกด้วย
```

# How do you secure the following?
- application
```javascript
1. domain name ควรทำ SSL เพื่อป้องกันการ Hack ระหว่าง client และ Server ผมชอบใช้ cloudflare
2. พวกช่องที่กรอกข้อมูลผ่านหน้าเว็บ ควรระวังการโดน Hack อาจจะทำการ encode ด้วย string ก่อนส่งขึ้น server แล้วจัดเก็บใส่ DB ที่เป็น SQL
3. ควรทำ sec scan ผ่าน gitlab ci ก่อน deploy
4. การทำ error message ควรระวังเพราะการเป็นเป้าโจมตีได้
```
- infrastructure
```javascript
1. ควรใช้ PKCS แทนการใช้ besic auth ในการ remote เข้า server
2. service สำคัญๆไม่ควรนำออก pubic ip
```
- data
```javascript
1. เพิ่มระบบจัดการสิทธ์ในการเข้าถึงแต่ล่ะส่วนของข้อมูล
2. การ blackup ข้อมูล
```

# Base on your experience, how do you reduce your service downtime as much as possible during
- software upgrade
```javascript
1. ใช้ความสามารถของ Blue/Green Deployments หรือ Canary deployment ของ k8s
2. ใช้งาน gitlab ci และ argocd มาช่วยการทำ devsecops ก่อน deploy
```
- database migration
```javascript
ไม่มีประสบการณ์ ตรงนี้จริงๆครับ
```
- incident
```javascript
    1. ทำการ rollback ทันทีในกรณีที่เกิดจากโค้ดของเราใน version ล่าสุด
```

# How do you design your Kubernetes cluster? what DNS, CNI, ingression is being used? Why?
กรณีมี server 1 cluster
```javascript
    1. ทำการสร้าง namespace 2 ส่วน production และ deployment
    2. แยก deployment ตามหน้าที่ของแต่ล่ะ service สื่อสารกันด้วย ClusterIP
    4. ใช้งาน Ingress สำหรับติดต่อกับภายนอกโดยการใช้ domain ชี้มาที่ ClusterIP ที่ต้องการ forword
```
พอดีข้อนี้ไม่แน่ใจว่าตอบตรงคำถามไหม ไม่เข้าใจหลักการทำงาน CNI พึ่งเคยได้ยินชื่อครั้งแรก

# How do you measure service quality to give the best experience to your customer? (SLO, SLA)
ไม่มีประสบการณ์ ตรงนี้จริงๆครับ พึ่งเคยรู้จัก SLO และ SLA

# How do you design a highly scalable Kubernetes cluster and which Kubernetes objects are being used? (answer as drawio or your tool of choices)
https://www.canva.com/design/DAE8KAzthoU/YIMk38QBYYIkWn2umHh69g/view?utm_content=DAE8KAzthoU&utm_campaign=designshare&utm_medium=link&utm_source=publishpresent

